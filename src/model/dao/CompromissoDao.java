/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.util.List;
import model.domain.Cliente;
import model.domain.Compromisso;
import model.domain.Vendedor;

/**
 *
 * @author joao.rolim
 */
public interface CompromissoDao {

    List<Cliente> buscarClientes();

    List<Vendedor> buscarVendedores();

    void exluir(Compromisso compromisso);

    List<Compromisso> pesquisaGeral(Compromisso compromisso);

    void salvarAtualizar(Compromisso compromisso);
    
}
