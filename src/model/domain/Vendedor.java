/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.domain;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import util.ValidacaoException;

/**
 *
 * @author joao.rolim
 */
@Entity
@Table(name="TB_VENDEDOR")
public class Vendedor implements Serializable {
    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="CD_VENDEDOR")
    private Long Id;
    private String nome;
    private String email;
    private String telefone;
    private String gerente;

    public Vendedor(){
        
    }
    
    public Long getId() {
        return Id;
    } 
    

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getGerente() {
        return gerente;
    }

    public void setGerente(String gerente) {
        this.gerente = gerente;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.Id);
        hash = 71 * hash + Objects.hashCode(this.nome);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vendedor other = (Vendedor) obj;
        if (!Objects.equals(this.Id, other.Id)) {
            return false;
        }
        if (!Objects.equals(this.nome, other.nome)) {
            return false;
        }
        return true;
    }

    public void validacao() throws ValidacaoException  {
            if (this.nome == null || this.nome.equals("")) {
            throw new ValidacaoException("Campo Nome Vazio!") ;
        }
         
         if (this.email == null || this.email.equals("")) {
            throw new ValidacaoException("Campo Email Vazio!") ;
        }
    }
    
    
    
    
}
