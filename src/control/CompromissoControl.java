/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;
import model.dao.CompromissoDao;
import model.dao.CompromissoDaoImp;
import model.domain.Cliente;
import model.domain.Compromisso;
import model.domain.Vendedor;
import model.service.ServiceLocator;
import org.jdesktop.observablecollections.ObservableCollections;

/**
 *
 * @author joao.rolim
 */
public final class CompromissoControl {
    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
    private Compromisso compromissoDigitado;
    private Compromisso compromissoSelecionado;
    private List<Vendedor> vendedorTabela;
    private List<Cliente> clienteTable;
    private List<Compromisso> compromissoTabela;
    private final CompromissoDao compromissoDao;

    public CompromissoControl() {
        compromissoDao = ServiceLocator.getCompromissoDao();
        compromissoTabela = ObservableCollections.observableList(new ArrayList<Compromisso>());
        clienteTable = ObservableCollections.observableList(new ArrayList<Cliente>());
        clienteTable.addAll(compromissoDao.buscarClientes());
        
        vendedorTabela = ObservableCollections.observableList(new ArrayList<Vendedor>());
        vendedorTabela.addAll(compromissoDao.buscarVendedores());
        
        
        novo();
        pesquisar();
    }

    public Compromisso getCompromissoDigitado() {
        return compromissoDigitado;
    }

    public void setCompromissoDigitado(Compromisso compromissoDigitado) {
        Compromisso oldcompromissoDigitado = this.compromissoDigitado;
        this.compromissoDigitado = compromissoDigitado;
        propertyChangeSupport.firePropertyChange("compromissoDigitado",oldcompromissoDigitado,compromissoDigitado);
    }

    public Compromisso getCompromissoSelecionado() {
        return compromissoSelecionado;
    }

    public void setCompromissoSelecionado(Compromisso compromissoSelecionado) {
        this.compromissoSelecionado = compromissoSelecionado;
        if (this.compromissoSelecionado != null) {
            setCompromissoDigitado(compromissoSelecionado);
        }
        
    }

    public List<Compromisso> getCompromissoTabela() {
        return compromissoTabela;
    }

    public void setCompromissoTabela(List<Compromisso> compromissoTabela) {
        this.compromissoTabela = compromissoTabela;
    }
    
    
    public void addPropertyChangeListener(PropertyChangeListener property){
       propertyChangeSupport.addPropertyChangeListener(property);
    }
    public void removePropertyChangeListener(PropertyChangeListener property){
        propertyChangeSupport.removePropertyChangeListener(property);
    }

    public void novo() {
        setCompromissoDigitado(new Compromisso());
    }
    
    
    public void salvar(){
        compromissoDao.salvarAtualizar(compromissoDigitado);
    }
    
    public void excluir(){
        compromissoDao.exluir(compromissoSelecionado);
    }

    public void pesquisar() {
        compromissoTabela.clear();
        compromissoTabela.addAll(compromissoDao.pesquisaGeral(compromissoDigitado));
    }
/*
    public List<Vendedor> getVendedorTabela() {
        //return vendedorTabela;
        vendedorTabela = ObservableCollections.observableList(new ArrayList<Vendedor>());
        vendedorTabela.addAll(compromissoDao.buscarVendedores());
        return vendedorTabela;
    }

    public void setVendedorTabela(List<Vendedor> vendedorTabela) {
        //Vendedor vendedor = this.vendedorTabela.
        this.vendedorTabela = vendedorTabela;
    }
    */

    public List<Cliente> getClienteTable() {
        /*List<Cliente> cli = new ArrayList<Cliente>();
        for(Cliente c : clienteTable){
            c.getNome();
            cli.add(c);           
        }
        return cli;   */
        return this.clienteTable;
    }
/*
    public void setClienteTable(List<Cliente> clienteTable) {
        this.clienteTable = clienteTable;
    }
*/
  
    
}
