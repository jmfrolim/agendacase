/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.beans.PropertyChangeSupport;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import model.dao.VendedorDao;
import model.domain.Vendedor;
import model.service.ServiceLocator;
import org.jdesktop.observablecollections.ObservableCollections;
import util.ValidacaoException;

/**
 *
 * @author joao.rolim
 */
public class VendedorControl {
    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
    private Vendedor vendedorDigitado;
    private Vendedor vendedorSelecionado;
    private List<Vendedor> vendedoresTabela;
    private final  VendedorDao vendedorDao;
    
    public VendedorControl(){
        vendedorDao = ServiceLocator.getVendedorDao();
        vendedoresTabela = ObservableCollections.observableList(new ArrayList<Vendedor>());       
        novo();
        pesquisar();       
    }
    
        public final void novo() {
        setVendedorDigitado(new Vendedor());
    }

    public final  void pesquisar() {
        vendedoresTabela.clear();
        vendedoresTabela.addAll(vendedorDao.pesquisar(vendedorDigitado));
    }
    
    public void salvar() throws ValidacaoException{
        vendedorDigitado.validacao();
        vendedorDao.salvarAtualizar(vendedorDigitado);
        novo();
        pesquisar();
    }
    
    public void excluir(){
        vendedorDao.excluir(vendedorDigitado);
        novo();
        pesquisar();
    }

    public Vendedor getVendedorDigitado() {
        return vendedorDigitado;
    }

    public void setVendedorDigitado(Vendedor vendedorDigitado) {
        Vendedor  oldvendedorDigitado = this.vendedorDigitado;
        this.vendedorDigitado = vendedorDigitado;
        propertyChangeSupport.firePropertyChange("vendedorDigitado", oldvendedorDigitado,vendedorDigitado);
    }

    public void setVendedorSelecionado(Vendedor vendedorSelecionado) {
        this.vendedorSelecionado = vendedorSelecionado;
        if (this.vendedorSelecionado != null) {
            setVendedorDigitado(vendedorSelecionado);
        }
    }

    public Vendedor getVendedorSelecionado() {
        return vendedorSelecionado;
    }
    
    

    public List<Vendedor> getVendedoresTabela() {
        return vendedoresTabela;
    }

    public void setVendedoresTabela(List<Vendedor> vendedoresTabela) {
        this.vendedoresTabela = vendedoresTabela;
    }
    
    
       public void addPropertyChangeListener(PropertyChangeListener property){
        propertyChangeSupport.addPropertyChangeListener(property);
    }
    
    public void removePropertyChangeListener(PropertyChangeListener property){
        propertyChangeSupport.removePropertyChangeListener(property);
    }      

    
}
