/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import model.domain.Cliente;
import model.domain.Compromisso;
import model.domain.Vendedor;

/**
 *
 * @author joao.rolim
 */
public class CompromissoDaoImp implements CompromissoDao {
    
    
    @Override
    public void salvarAtualizar(Compromisso compromisso){
        EntityManager em = Conexao.getEntityManager();
        em.clear();
        em.getTransaction().begin();
        if (compromisso.getId() != null) {
            compromisso = em.merge(compromisso);       
        }
        em.persist(compromisso);
        em.getTransaction().commit();
        em.close();
    }
    
    @Override
    public List<Compromisso> pesquisaGeral(Compromisso compromisso){
          EntityManager em = Conexao.getEntityManager();

        StringBuilder sql = new StringBuilder("from Compromisso co where 1 = 1 ");

        if (compromisso.getId() != null) {
            sql.append("and co.id=:id ");
        }

        if (compromisso.getDataCompromisso()!= null && !compromisso.getDataCompromisso().equals("")) {
            sql.append("and co.dataCompromisso like:dataCompromisso ");
        }
        
        if(compromisso.getClientes()!=null){
            sql.append("and co.cliente.codigo=: codigoCliente");
        }
            
        Query query = em.createQuery(sql.toString());
        if (compromisso.getId() != null) {
            query.setParameter("id", compromisso.getId());
        }
        if (compromisso.getDataCompromisso()!= null) {
            query.setParameter("data", "%"+compromisso.getDataCompromisso()+"%");
        }
        
        if (compromisso.getClientes()!=null) {
            query.setParameter("codigoCliente",compromisso.getClientes().getId());
            
        }
        return query.getResultList();
    }     
    
    @Override
    public void  exluir(Compromisso compromisso){
        EntityManager em = Conexao.getEntityManager();
        em.clear();
        em.getTransaction().begin();
        compromisso = em.merge(compromisso);
        em.remove(compromisso);
        em.getTransaction().commit();
        em.close();        
    }
    
    
    @Override
    public List<Cliente> buscarClientes(){
        ClienteDao clienteDao = new ClienteDaoImp();
        return clienteDao.pesquisar(new Cliente());
    }
    
    @Override
    public List<Vendedor> buscarVendedores(){
        VendedorDao vendedorDao = new VendedorDaoImp();
        Vendedor v = new Vendedor();
        return vendedorDao.pesquisar(v);
    }
}
