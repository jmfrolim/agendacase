/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import model.domain.Vendedor;

/**
 *
 * @author joao.rolim
 */
public class VendedorDaoImp implements VendedorDao{

    @Override
    public void salvarAtualizar(Vendedor vendedor) {
        EntityManager em = Conexao.getEntityManager();
        em.getTransaction().begin();
        if (vendedor.getId() != null) {
            vendedor = em.merge(vendedor);
        }
        em.persist(vendedor);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void excluir(Vendedor vendedor) {
        EntityManager em = Conexao.getEntityManager();
        em.getTransaction().begin();
        vendedor = em.merge(vendedor);
        em.remove(vendedor);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public List<Vendedor> pesquisar(Vendedor vendedor) {
        EntityManager em = Conexao.getEntityManager();

        StringBuilder sql = new StringBuilder("from Vendedor v where 1 = 1 ");

        if (vendedor.getId() != null) {
            sql.append("and v.id=:id ");
        }

        if (vendedor.getNome() != null && !vendedor.getNome().equals("")) {
            sql.append("and v.nome like:nome ");
        }
        Query query = em.createQuery(sql.toString());
        if (vendedor.getId() != null) {
            query.setParameter("id", vendedor.getId());
        }
        if (vendedor.getNome() != null) {
            query.setParameter("nome", "%"+vendedor.getNome()+"%");
        }
        return query.getResultList();
    }

}
