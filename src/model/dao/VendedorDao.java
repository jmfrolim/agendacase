/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import java.util.List;
import model.domain.Vendedor;

/**
 *
 * @author joao.rolim
 */
public interface VendedorDao {

    void excluir(Vendedor vendedor);

    List<Vendedor> pesquisar(Vendedor vendedor);

    void salvarAtualizar(Vendedor vendedor);
    
}
