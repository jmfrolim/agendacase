/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.domain;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import util.ValidacaoException;

/**
 *
 * @author joao.rolim
 */
@Entity
@Table(name = "TB_CLIENTE")
public class Cliente implements Serializable {
    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="CD_CLIENTE")
    private Long id;
    private String nome;
    private String telefone;
    private String email;           

    public Cliente() {
    }

    public Long getId() {
        return id;
    }
/*
    public void setId(Long id) {
        this.id = id;
    }*/

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.id);
        hash = 53 * hash + Objects.hashCode(this.nome);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cliente other = (Cliente) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public void validacao ()throws ValidacaoException{
         if (this.nome == null || this.nome.equals("")) {
            throw new ValidacaoException("Campo Nome Vazio!") ;
        }
         
         if (this.email == null || this.email.equals("")) {
            throw new ValidacaoException("Campo Email Vazio!") ;
        }
    }
    
    
    
    
    
}
