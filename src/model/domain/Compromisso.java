/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.domain;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author joao.rolim
 */

@Entity
@Table(name="TB_COMPROMISSO")
public class Compromisso implements Serializable {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="CD_COMPROMISSO")
    private Long id;
    //private String data;
    @ManyToOne
    @JoinColumn(name="CD_VENDEDOR",nullable=false)
    private Vendedor vendedor;
    private String descricao;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar dataCompromisso;        
    private boolean status;    
    
    @ManyToOne
    @JoinColumn(name="CD_CLIENTE",nullable=false)
    private Cliente clientes;

    public Compromisso() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Calendar getDataCompromisso() {
        return dataCompromisso;
    }

    public void setDataCompromisso(Calendar dataCompromisso) {
        this.dataCompromisso = dataCompromisso;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Cliente getClientes() {
        return clientes;
    }

    public void setClientes(Cliente clientes) {
        this.clientes = clientes;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.id);
        hash = 89 * hash + Objects.hashCode(this.vendedor);
        hash = 89 * hash + Objects.hashCode(this.dataCompromisso);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Compromisso other = (Compromisso) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.vendedor, other.vendedor)) {
            return false;
        }
        if (!Objects.equals(this.dataCompromisso, other.dataCompromisso)) {
            return false;
        }
        return true;
    }
    
    
    
    
    
}
