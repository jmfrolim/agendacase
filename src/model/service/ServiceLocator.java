/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.service;

import model.dao.ClienteDao;
import model.dao.ClienteDaoImp;
import model.dao.CompromissoDao;
import model.dao.CompromissoDaoImp;
import model.dao.VendedorDao;
import model.dao.VendedorDaoImp;

/**
 *
 * @author joao.rolim
 */
public class ServiceLocator {

    public static ClienteDao getClienteDao() {
       return new ClienteDaoImp();
    }
    
    public static VendedorDao getVendedorDao(){
        return new VendedorDaoImp();
    }
    
    public static CompromissoDao getCompromissoDao(){
        return new CompromissoDaoImp();
    }
    
    
}
